package PatroneDeDiseño.factory_2;

public class App {

    public static void main(String[] args) {

        ConexionFabrica fabrica = new ConexionFabrica();

        Iconexion cx1 = fabrica.getConexion("POSTGRESQL");
        cx1.conectar();
        cx1.desconectar();
        Iconexion cx2 = fabrica.getConexion("ORACLE");
        cx2.conectar();
        cx1.desconectar();
        Iconexion cx3 = fabrica.getConexion("vacia");
        cx3.conectar();
        cx3.desconectar();
    }


}
