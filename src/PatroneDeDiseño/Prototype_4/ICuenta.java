package PatroneDeDiseño.Prototype_4;

public interface ICuenta extends Cloneable{
    ICuenta clonar();
}