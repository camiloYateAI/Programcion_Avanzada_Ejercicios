package PatroneDeDiseño.Prototype_4;

public class App {

    public static void main(String[] args) {
        CuentaAHimplementacion cuentaAhorro = new CuentaAHimplementacion();
        cuentaAhorro.setMonto(200);
        CuentaAHimplementacion cuentaClonada = (CuentaAHimplementacion) cuentaAhorro.clonar();

        if (cuentaClonada != null) {
            System.out.println(cuentaClonada);
        }

        // comparando referencias en memoria y no en valores por eso sale $false
        System.out.println(cuentaClonada == cuentaAhorro);
    }
}
