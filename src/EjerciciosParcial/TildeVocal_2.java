package EjerciciosParcial;

import java.util.Scanner;

public class TildeVocal_2 {

    public static void main(String[] args) {


        /*
        * Realice un Programa Java, que lea una vocal (como un String) y muestre si esa vocal:
             Esta o no tildada
             Esta o no en mayúscula
        *é, á, í, ó, ú , Á , É , Í, , Ó , Ú
        * */
        Scanner in = new Scanner(System.in);
        System.out.println("Digite una letra");
        String letra = in.next();

        if (letra.equals("a") || letra.equals("e") || letra.equals("i") ||
                letra.equals("o") || letra.equals("u")) {
            System.out.println(letra + " es vocal");
        }
        else if (letra.equals("A") || letra.equals("E") || letra.equals("I") ||
                letra.equals("O") || letra.equals("U")) {
            System.out.println("la " + letra + " es vocal y esta en Mayuscula");

        }
        else if (letra.equals("é") || letra.equals("á") || letra.equals("í") ||
                letra.equals("ó") || letra.equals("ú")) {
            System.out.println("Esta vocal " + letra + " tiene tilde y es minuscula");

        }

        else if (letra.equals("Á") || letra.equals("É") || letra.equals("Ó") ||
                letra.equals("Í") || letra.equals("Ú")) {
            System.out.println("Esta vocal " + letra + " tiene tilde y es Mayuscula");
        } else {
            System.out.println(letra + " no es vocal");
        }


    }
}
